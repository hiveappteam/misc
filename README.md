# hive notes
## api
### ```api/v1/tags```
+ GET --> отсортированный список тегов по votes и timestamp (limit как query параметр)
```{ id:<>, name:<>, url:<>, brief:<>, votes:<>, timestamp:<> }```
+ POST --> добавить тег в список для голосования
```{ name:<>, url:[], brief:[] }```
### ```api/v1/tags/<id>```
+ GET --> объект с полной информацией (как-то будет отличаться объектов в списке тегов?)
+ PUT --> upvote

## rate limit
надо защититься от большого количества запросов от одного клиента, для этого можно использовать jwt, выдавая его через отдельный сервис и отзывать (через удаление из whitelist) после превышения лимита на запросы в единицу времени

## voting
+ one email --> one vote per id
+ подтверждение по email + генерация ссылок для подтверждения
+ one telegram user --> one vote

## telegram bot
+ напилить *inline bot* в develop?